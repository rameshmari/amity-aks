terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.60.0"
    }
    random = {
      source = "hashicorp/random"
      version = "3.1.0"
    }
  }
}
provider "azurerm" {
  features {}

}
locals {
  aks_cluster_name    = "aks-${local.resource_group_name}"
  location            = "Central India"
  resource_group_name = "ramesh-amity-resources"
}
#resource-group.tf
resource "azurerm_resource_group" "primary" {
  name     = local.resource_group_name
  location = local.location
}
#log-analytics.tf
resource "azurerm_log_analytics_workspace" "insights" {
  name                = "logs-${random_pet.primary.id}"
  location            = azurerm_resource_group.primary.location
  resource_group_name = azurerm_resource_group.primary.name
  retention_in_days   = 30
}

#aks-administrators-group.tf
resource "azuread_group" "aks_administrators" {
display_name        = "${local.aks_cluster_name}-administrators"
description = "Kubernetes administrators for the ${local.aks_cluster_name} cluster."
}

#aks-versions.tf
data "azurerm_kubernetes_service_versions" "current" {
  location = azurerm_resource_group.primary.location
}
#aks-cluster.tf
resource "azurerm_kubernetes_cluster" "aks" {
  dns_prefix          = local.aks_cluster_name
  kubernetes_version  = data.azurerm_kubernetes_service_versions.current.latest_version
  location            = azurerm_resource_group.primary.location
  name                = local.aks_cluster_name
  node_resource_group = "${azurerm_resource_group.primary.name}-aks"
  resource_group_name = azurerm_resource_group.primary.name

  addon_profile {
    azure_policy { enabled = true }

    oms_agent {
      enabled                    = true
      log_analytics_workspace_id = azurerm_log_analytics_workspace.insights.id
    }
  }

  default_node_pool {
    enable_auto_scaling  = false
    node_count           = 1
    name                 = "system"
    orchestrator_version = data.azurerm_kubernetes_service_versions.current.latest_version
    os_disk_size_gb      = 1024
    vm_size              = "Standard_DS2_v2"
  }

  identity { type = "SystemAssigned" }

  role_based_access_control {
    enabled = true
    azure_active_directory {
      managed                = true
     admin_group_object_ids = [azuread_group.aks_administrators.object_id]

    }
  }

}
  resource "azuread_group_member" "example" {
    group_object_id  = azuread_group.aks_administrators.id
    member_object_id = "6de76639-5ec2-42f4-8e9c-554223fce588"
  }


resource "random_pet" "primary" {}
